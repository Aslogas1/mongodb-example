import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.ClassModel;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.*;


public class Main {

    private static final String MONGOIMPORT = "src/main/resources/mongoimport.exe";
    private static final String CSVFILE = "src/main/resources/mongo.csv";

    public static void main(String[] args) {

        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder()
                        .register(
                                ClassModel.builder(Student.class).enableDiscriminator(true).build()
                        ).automatic(true)
                        .build()
                )
        );
        MongoCollection<Student> students = new MongoClient("127.0.0.1", 27017).getDatabase("database")
                .withCodecRegistry(codecRegistry).getCollection("students", Student.class);
        importDb();
        Set<Student> studentsElderFortyYearsAmount = students.find(Filters.gt("age", 40)).into(new HashSet<>());
        System.out.println("Общее количество студентов в базе: " +
                students.find(Filters.gt("age", 0)).into(new HashSet<>()).size());
        System.out.println("Количество студентов старше 40 лет: " + studentsElderFortyYearsAmount.size());
        System.out.println("Имя самого молодого студента " +
                Objects.requireNonNull(students.find(Filters.eq("age", 18)).first()).getName());
        System.out.println("список курсов самого старого студента: " + studentsElderFortyYearsAmount.stream()
                .max(Comparator.comparing(Student::getAge)).get().getCourses());


    }

    private static void importDb() {
        Runtime runtime = Runtime.getRuntime();
        String command = Main.MONGOIMPORT + " --db database --collection students --fields name,age,courses --type csv --file " + Main.CSVFILE;
        try {
            runtime.exec(command);
            System.out.println("Reading csv into Database");
        } catch (Exception e) {
            System.out.println("Error executing " + command + e.getMessage());
        }
    }
}